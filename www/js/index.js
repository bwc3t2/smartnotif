/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var app = {

    tableHeaders: {
        timestamp: 'Time Sent',
        callerNumber: 'Caller',
        recipientEmail: 'Recipient',
        emailSent: 'Sent',
    },
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        document.getElementById('email').addEventListener('blur', this.setEmail.bind(this), false);
        document.getElementById('settingsForm').addEventListener('submit', this.setEmail.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var self = this;
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');
        var ringedFlag = false;

        var recipientEmail = window.localStorage.getItem('email');
        document.getElementById('email').value = recipientEmail;
        self.populateLogsTable(self.getLogs());

        var getLastNumber = function(callback) {
            if(window.plugins.calllog != "undefined"){
                window.plugins.calllog.list(1, function (response) {
                    var lastNumber = 0;
                    if (response.rows[0].type == 3)
                        lastNumber = response.rows[0].number;
                    callback(lastNumber);
                });
            }
        }
        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
        PhoneCallTrap.onCall(function(state) {
            switch(state) {
                case "RINGING":
                    receivedElement.innerHTML = "RINGING";
                    ringedFlag = true; 
                    break;
                case "OFFHOOK":
                    receivedElement.innerHTML = "-";
                    break;
                case "IDLE":
                    if (ringedFlag) {
                        receivedElement.innerHTML = "IDLE";
                        setTimeout(function() {
                            getLastNumber(function(lastNumber) {
                                var a = "You have a missed call from " + lastNumber;
                                var url = "http://evomg.com/mailsender/index.php?to=" + recipientEmail + "&from=arnel@evomg.com&subject=NOTIFICATION&messages="+ a
                                cordovaHTTP.get(url, {}, {}, function(response) {
                                    self.addCallToLogs({
                                        timestamp: new Date().getTime(),
                                        callerNumber: lastNumber,
                                        recipientEmail: recipientEmail,
                                        emailSent: true
                                    });
                                }, function(response) {
                                    self.addCallToLogs({
                                        timestamp: new Date().getTime(),
                                        callerNumber: lastNumber,
                                        recipientEmail: recipientEmail,
                                        emailSent: false
                                    });
                                });
                            });
                        }, 10000);
                        ringedFlag = false;
                    }
                    break;
                default:
                    receivedElement.innerHTML = state;
            }
        });
        cordova.plugins.email.addAlias('gmail', 'com.google.android.gm');
        cordova.plugins.email.isAvailable(
            function (isAvailable) {
                if (isAvailable)
                 alert('Service is not available.');
            }
        );

    },

    setEmail: function () {
        window.localStorage.setItem('email', document.getElementById('email').value);
    },

    getLogs: function () {
        var calls = window.localStorage.getItem('calls');
        if (calls)
            calls = JSON.parse(calls);
        else
            calls = [];
        return calls;
    },

    addCallToLogs: function (logData) {
        var logs = this.getLogs();
        logs.push(logData);
        window.localStorage.setItem('calls', JSON.stringify(logs));
        this.populateLogsTable(logs);
        // alert(window.localStorage.getItem('calls'));
    },

    populateLogsTable: function (calls) {
        var divContainer = document.getElementById("logs");
        divContainer.innerHTML = "";
        if (Array.isArray(calls) && calls.length > 0) {
            var col = [];
            for (var i = 0; i < calls.length; i++) {
                for (var key in calls[i]) {
                    if (col.indexOf(key) === -1) {
                        col.push(key);
                    }
                }
            }

            var table = document.createElement("table");
            var tr = table.insertRow(-1);

            for (var i = 0; i < col.length; i++) {
                var th = document.createElement("th");
                th.innerHTML = this.tableHeaders[col[i]];
                tr.appendChild(th);
            }

            for (var i = 0; i < calls.length; i++) {
                tr = table.insertRow(-1);
                for (var j = 0; j < col.length; j++) {
                    var tabCell = tr.insertCell(-1);
                    if(j === 0)
                        tabCell.innerHTML = new Date(calls[i][col[j]]);
                    else
                        tabCell.innerHTML = calls[i][col[j]];
                }
            }
            divContainer.appendChild(table); 
        } else {
            divContainer.innerHTML = "No calls in history.";
        }
       
    }
};

app.initialize();