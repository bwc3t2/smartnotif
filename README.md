# README #

This app listens to missed calls made to your mobile phone. It then sends email to set email address as a notification of the missed call.

### Challenge Name & No: ###
3 - Secure Room Notifications.(Smartnotif)

### Name Of The Persons: ###
Arnel Clave
James Necio
Jerald Tarroquin
Paul Sarcia

### Challenge Use Case Description: ###
If any calls comes in mobile – it should send an email to their mail address saying you got  a missed call from X Number @ Y date time.


### Technology Stack: ###
* Cordova – mobile app
* Php – Mail server
* Web Technologies – for building the actual app
* Heroku – for mail server deployment
* Bitbucket(Git) – for project maintenance for team ###
* Slack-Bitbucket(Git) Integration – for Git notifications and build notifications


### Working Solution Video: ###
[https://files.slack.com/files-pri/T43HVTD0T-F4S1EMJDB/download/2017-03-30-17-06-57.mp4](Link URL)


### Mentor Name: ###
The wonderful world of the internet 